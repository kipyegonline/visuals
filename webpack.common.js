const Webpack=require('webpack');
const HmtlWebpackPlugin=require('html-webpack-plugin');
const MomentLocalesPlugin = require('moment-locales-webpack-plugin');
module.exports={
    entry:"./src/js/app.js", 
   module:{
        rules:[
            {
                test:/\.js$/,
                exclude:/node_modules/,
                use:[
                   { loader:"babel-loader",
                    options:{
                        presets:['@babel/preset-env','@babel/preset-react',"babel-preset-airbnb"]}
                   }
                
                ]
            },
            /*
            
            {
                test:/\.html$/,
                use:['html-loader']
            },*/
            {
                test:/\.(csv|jpeg|jpg|png|svg)$/,
                use:[
                    {loader:'file-loader',
            options:{
                name:"[name].[ext]",
                outputPath:"/img",
                publicPath:"./"
            }
        }]
            }//file
        ]
    },//module
    plugins:[

        new Webpack.ProvidePlugin({
        $:'jquery',jQuery:'jquery',
        React:"react"
    }),
        new MomentLocalesPlugin(),
        new Webpack.HotModuleReplacementPlugin(),
        new HmtlWebpackPlugin({template:"./index.html",filename:"index.html"}),
        new HmtlWebpackPlugin({template:"./census.html",filename:"census.html"}),
        new HmtlWebpackPlugin({template:"./areachart.html",filename:"areachart.html"}),
        new HmtlWebpackPlugin({template:"./stacked.html",filename:"stacked.html"}),
        new HmtlWebpackPlugin({template:"./scatter.html",filename:"scatter.html"}),
        new HmtlWebpackPlugin({template:"./march.html",filename:"march.html"}),
        new HmtlWebpackPlugin({template:"./line.html",filename:"line.html"}),
        new HmtlWebpackPlugin({template:"./pie.html",filename:"pie.html"}),
    ],
    
};
