module.exports={
    "extends":["prettier","airbnb","plugin:react/recommended","eslint:recommended"],
    "env":{
        "browser":true,
        "node":true,
        "jquery":true,
        "commonjs":true,
        "es6":true,
        "jest":true
    },
    "parserOptions": {
        "ecmaVersion": 6,
        "sourceType": "module",
        "ecmaFeatures": {
            "jsx": true
        }
    },
    "plugins":["prettier","react"],
    "rules": {
        "semi": ["error", "always"],
        "eqeqeq":[2,"always"],
        "indent":[2,4,{ "VariableDeclarator": { "var": 2, "let": 2, "const": 3 }} ],
        "radix":[2,"as-needed"],
        "yoda":["error","always"],
        "no-undef":"error",
        "no-unused-vars":"error",
        "no-irregular-whitespace": "error",
        "no-else-return": ["error", {allowElseIf: false}],
        "arrow-body-style": ["error", "as-needed"],
        "jsx-quotes": ["error", "prefer-double"],
        "no-plus-plus":0,
        "quotes": ["error", "double"],
        "comma-dangle":0
    }
    

}