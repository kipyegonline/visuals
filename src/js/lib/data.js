import * as d3 from "d3";
let countyData = []
const sanitizeData = str => Number(str.split(",").join(""));

export default function getData(file) {

    d3.csv(file)
        .then(data => {

            countyData = data;
            countyData = countyData.map(data => {
                data["Total_Population19"] = sanitizeData(data['Total_Population19'])
                data["Male populatio 2019"] = sanitizeData(data['Male populatio 2019'])
                data["Female population 2019"] = sanitizeData(data['Female population 2019'])
                data["Population in 2009"] = sanitizeData(data['Population in 2009'])
                data["Pop_change"] = sanitizeData(data['Pop_change'])
                data["Households"] = sanitizeData(data['Households'])
                data["Av_HH_Size"] = sanitizeData(data['Av_HH_Size'])
                data["LandArea"] = sanitizeData(data['LandArea'])
                data["Population Density"] = sanitizeData(data['Population Density'])
                data["Intersex population 2019"] = sanitizeData(data['Intersex population 2019'])
                return data;
            })


            console.log('counties', data)
        })
        .catch(err => console.log(err))
}

export async function getNTSA(file) {
    const data = await d3.csv(file)
    console.log(data)


}

export { countyData };