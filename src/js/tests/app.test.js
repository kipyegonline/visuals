
function makeProm(){
    const prom=new Promise((resolve,reject)=>{
        setTimeout(()=>{
   if(Math.floor(Math.random() * 10) % 2===0){
       resolve("EVen  Number");
   }else{
       reject("Odd number");
   }
        },5000);
    });
    return prom;
}


describe("Modulo",()=>{
    beforeAll(()=>{
        test('fatness',()=>{
            expect(2+3).toEqual(4)
        })
    })
    test('Evenness',()=>{
       return  expect(makeProm()).resolves.toBe("EVen  Number")
    });

    test('callback hell',()=>{
        return makeProm().then(res=>verify(res));
        function verify(data){
            expect(data).toBe("EVen  Number");
            
            }
    })
});