import moment from "moment";
import React from 'react'
import ReactDOM from 'react-dom'
import "../../node_modules/materialize-css/dist/js/materialize.min";

import charting from './components/charts'
//import runLine from './components/line'
import getData, { countyData, getNTSA } from "./lib/data";
import runPage from "./components/feb"
import runStacks from "./components/stackedbar"
const census = require("../assets/census/2019-population_census-report-per-county.csv");
const ntsa = require("../assets/NTSA/DAILY REPORT AS AT 17TH FEBRUARY 2020.csv");

console.log(census);
/* eslint-disbale imports: "off" */
import "materialize-css/sass/materialize.scss";
import "../css/main.css";
import { Pie } from "./components/pie";

//initialize data







function Vince() {
    return (<div>
        <p>Do you mean what you say....</p>
    </div>)
}

//ReactDOM.render(<Vince/>,document.getElementById('root'))

//pull the data from excel files



document.addEventListener("DOMContentLoaded", function () {
    getData(census);
    getNTSA(ntsa)
    setTimeout(() => {

        runPage(countyData);
        runStacks(countyData);
        charting(countyData);
        Pie(countyData);
    }, 500);
});