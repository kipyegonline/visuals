
//aqua alice blue robin egg blue tiffany blue 
//deepskyblue
//ghostwhite
//indianred
//khaki
//lavender
//lightcyan
///lime
//moccasin
//siena
//snow
//tomato
const file = '../assets/Facebook Insights Data Export (Post Level) - Public Relations Society of Kenya - PRSK - 2019-04-05.csv'
const h = 400, w = 600, gh = 300, gw = 480;
let dataset = [];
const margin={right:20,left:100,top:20,bottom:80}
const svg = d3.select("#area-root")
    .append('svg')
    .attr('height', h)
    .attr('width', w)
    .style('background', "deepskyblue");
const graph = svg.append("g")
    .attr('height', gh)
    .attr('width', gw)
    .attr('transform', `translate(${margin.left},${margin.top})`);
/*SCALES AND AXIS */
      //scales
      const xScale = d3.scaleTime().rangeRound([0, gw]);
      const yScale = d3.scaleLinear().rangeRound([gh, 0]);
      //axis
      const yaxis = d3.axisLeft(yScale).ticks(5).tickFormat(d=> d >0 ? d.toLocaleString() :"")
      const xaxis=d3.axisBottom(xScale)
      
      const ygrp = graph
              .append('g')
              .attr('class','ygrp')
      const xgrp = graph
          .append('g')
          .attr('transform', `translate(0,${gh})`)
    .attr('class', 'xgrp')
const areaChart = (d) =>
    d3.area()
.x(d => xScale(new Date(d.Posted)))
.y0(()=>yScale.range()[0])
.y1(d => yScale(d.impressions))
function createAreas(dataset) {
        
    
        xScale.domain(d3.extent(dataset, d => new Date(d.Posted)));
        yScale.domain([0, d3.max(dataset, d => d.impressions)]);
    
        areaChart(dataset)
    //add path
        
        graph.append('path')
            .data([dataset])
            .on('mouseenter', handleMouseOver)
            .on('mouseleave',handleMouseLeave)
            .transition()
            .duration(1000)
            .attr('class', 'area')
            .attr('d',areaChart())
            .attr('fill', 'rebeccapurple')
            .attr('stroke','none')
            .attr('stroke-width', 2)
        //create circles
        const circles=graph.selectAll('circle')
        .data(dataset)
        //remove nwanted
        circles.exit().remove()
        //update current points
        circles.attr('r',4)
        .attr('cx',d=>xScale(new Date(d.Posted)))
        .attr('cy',d=>yScale(d.impressions))
        
        
    
    //add new points
        circles.enter()
            .append('circle')
            .on('mouseover',(d,i,n)=>{
                d3.select(n[i])
                .transition()
                .duration(100)
                .attr('r',8)
                    .attr('fill', 'red')
                const selected= d3.select(n[i])
                //handleMouseOver(selected,d)
            })
            .on('mouseleave',(d,i,n)=>{
                d3.select(n[i])
                .transition()
                .duration(100)
                .attr('r',4)
                    .attr('fill', 'none')
                    const selected= d3.select(n[i])
                   // handleMouseLeave(selected,d)
            })
        .attr('r',4)
        .attr('cx',d=>xScale(new Date(d.Posted)))
        .attr('cy',d=>yScale(d.impressions))
            .attr('fill', 'none')
            .append('text')
            .text(d => d.impressions.toLocaleString())
            .attr('x',d=>xScale(new Date(d.Posted)) + 5)
            .attr('y', d => yScale(d.impressions) + 5)
            .attr('fill', 'black')
            .attr('font-size', '1rem')
        
    
        //call the axis
        xgrp
            .transition()
            .duration(1000)
            .call(xaxis)
        ygrp
            .transition()
            .duration(1000)
            .call(yaxis)
        
        ygrp.selectAll('.ygrp text')
            .attr('font-size', '1rem')
            .attr('fill', 'black')
            .attr('font-family', 'helvetica')
            .attr('x', -30);
        xgrp.selectAll('.xgrp text')
            .attr('font-size', '1rem')
            .attr('fill', 'black')
            .attr('font-family', 'helvetica')
            .attr('transform', 'rotate(-60)')
            .attr('text-anchor','end')
            .attr('x', -10);
        
        graph.append('text')
            .text('DATE')
            .attr('x', w / 3)
            .attr('y', gh + 80)
            .attr('fill', 'teal')
            .attr('letter-spacing', '5px');
        
        graph.append('text')
            .text('Impressions')
            .attr('x', -gh /1.5)
            .attr('y', -70)
            .attr('letter-spacing', '5px')
            .attr('fill', 'teal')
            .attr('transform', 'rotate(-90)');
}

const handleMouseOver = () => {
    d3.selectAll('circle').attr('fill','red')
}

const handleMouseLeave = () => {
    d3.selectAll('circle').attr('fill','none')
}

    async function getData(file){
        let data = await d3.csv(file)
        
        data
            .map(datam => {
               // datam.Posted=datam["Date"]
        datam.impressions = + datam['Lifetime Post Impressions by people who have liked your Page'];
        datam.totalImpressions = + datam['Lifetime Post Total Impressions'];
        datam.totalReach = + datam['Lifetime Post Total Reach'];
            return data;
        })
        //make dataset available globally
        dataset = data.slice(1)
        console.log('area', dataset)
        createAreas(dataset)
        
    }
        //invoke the damn function
    getData(file)