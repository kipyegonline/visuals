
import * as d3 from 'd3';
import {parseData} from './charts'
const filer=require('../../tweet_activity_metrics_kipyegOnline_20190601_20190701_en.csv')
export default function runLine(){

}//main func
    //dimensions
const w=400,h=400;
//margins
const margin={left:60,right:20,top:20,bottom:60}

let gh=h-margin.top-margin.bottom;
let gw=w-margin.right-margin.left;
//scales
const xscale=d3.scaleTime().range([0,gw])
const yscale=d3.scaleLinear().range([gh,0])
//select svg and add dims
const svg=d3.select('#line')
.append('svg')
.attr('width',w)
.attr('height',h)
.style('background','deepskyblue')

//append graph
const graph=svg.append('g')
.attr('height',gh)
.attr('width',gw)
.attr('transform',`translate(${margin.left},${margin.right})`)
//define axis
const xaxis=d3.axisBottom(xscale);
const yaxis=d3.axisLeft(yscale);
//define axis groups
const xgrp=graph.append('g')
.attr('class','xgrp')
.attr('transform',`translate(0,${gh})`)
const ygrp=graph.append('g')
.attr('class','y-axis')
//line 
const line=d3.line()
.x(d=>xscale(d.time))
.y(d=>yscale(d.impressions));


function addData(data){
yaxis.tickFormat(data=>data + ' K')
xscale.domain(d3.extent(data, d=>d.time))
yscale.domain(d3.extent(data, d=> d.impressions))

xgrp.call(xaxis);
ygrp.call(yaxis)

xgrp.selectAll('text')
.attr('transform','rotate(-50)')
.attr('text-anchor','end')
.attr('fill','blue')
.attr('fontSize','1rem');

ygrp.selectAll('text')
.attr('fill','teal')
.attr('fontSize','1rem')
.attr('font-weight','bold')
//path
graph.append('path')
.attr('stroke','red')
.attr('stroke-width','1px')
.attr('fill','none')
.attr('d',line(data))
}

d3.csv(filer)
.then(data=>parseData(data))
.then(data=>addData(data))




