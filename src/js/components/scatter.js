const file = '../assets/Facebook Insights Data Export (Post Level) - Public Relations Society of Kenya - PRSK - 2019-04-05.csv'
//define svg
const h = 600, w = 600, gw = 480, gh = 480, fill = "blue", hover = "green";
const margin = { top: 20, left: 100, right: 20, bottom: 100 };
let dataset = [];
//select DOM and append SVG
const svg = d3.select("#main-scat")
.append('svg')
.attr('height',h)
.attr('width', w)
.style('background','skyblue')
//append graph and coords
const graph = svg.append("g")
    .attr('height', gh)
    .attr('width', gw)
    .attr('transform', `translate(${margin.left},${margin.top})`)
    //define scales
const xScale = d3.scaleTime().rangeRound([0, gw]);
const yScale = d3.scaleLinear().rangeRound([gh, 0]);
const aScale = d3.scaleSqrt().rangeRound([0, 20])
const color= d3.scaleOrdinal().range(['green','blue','orange'])
//define axis
const xaxis = d3.axisBottom(xScale);
const yaxis=d3.axisLeft(yScale).ticks(5).tickFormat(d=> d.toLocaleString())
    
// axis groups
const xgrp = graph.append('g')
    .attr('class', 'xgrp')
    .attr('transform', `translate(0,${gh})`)
const ygrp = graph.append('g')
    .attr('class', 'ygrp')

    //events
const handleMouseLeave = (selected,d) => {
    selected.attr('fill','red')
    d3.select('#scat-tooltip')
        .style('opacity', 0)
        d3.select(".line").attr('stroke','none')
   
}
const handleMouseOver = (el,d) => {
    
    el.attr('fill', 'green')
    const top = Number(el.attr('cy'));
    const left = Number(el.attr('cx'))
    
    d3.select("#scat-views")        
        .text(d.impressions.toLocaleString())
        .style('font-weight','bold');
    d3.select('#scat-tooltip')
        .style('top',`${top + 30}px`)
        .style('left',`${left -10}px`)
        .style('opacity', 1)
    //activate line graph
    d3.select(".line").attr('stroke','red')
}
//call line graph
const makeLine = (dataset) => {
    const line = d3.line()
    .x(d => xScale(new Date(d.Posted)))
    .y(d => yScale(d.impressions));

graph.append('path')
    .data([dataset])
    .transition()
    .duration(1000)
    .attr('class', 'line')
    .attr('d',line)
    .attr('fill', 'none')
    .attr('stroke','none')
    .attr('stroke-width', 2)
} 
         
const areaChart = (dataset) => {
   const d= d3.area()
    .x(d => xScale(new Date(d.Posted)))
    .y0(()=>yScale.range()[0])
        .y1(d => yScale(d.impressions))
    
    
        graph.append('path')
        .data([dataset])       
        .transition()
        .duration(1000)
        .attr('class', 'area')
        .attr('d',d)
        .attr('fill', 'rebeccapurple')
        .attr('stroke','none')
        .attr('stroke-width', 2)
       }
       
   
function createGraph(dataset) {
    //map scale to domain
    xScale.domain(d3.extent(dataset, d =>new Date(d.Posted)));
    yScale.domain(d3.extent(dataset, d => d.impressions));
    aScale.domain(d3.extent(dataset, d => d.impressions));
    

//line graph
    makeLine(dataset);
    //areaChart(dataset);
    
    //LABELS
    const users = ["men", "women", "trans"]
    color.domain(users.map(user=>user))
    const label = graph.append("g")
       .attr('transform', `translate(${gw - 100},${gh / 4})`)        
        .attr('class', 'label')

    users.map((user,index) => {
        const legendRow=label.append("g")
        .attr("transform",`translate(0,${index *20})`)
        legendRow
        .append("rect")
        .attr('height',10)
        .attr('width',10)
        .attr('fill',color(user))

        legendRow
        .append('text')
        .attr('x',15)
        .attr('y',10)
        .attr('text-anchor','center')        
        .text(user.toLocaleUpperCase())
    })
    
    //selectcircles and append data
   
    const circles = graph
        .selectAll('circle')
        .data(dataset,d=>d['Post ID'])
    //remove used circles, if any
    circles.exit().remove();

    //update
    circles
    .transition()
    .duration(500)
        .attr('cx', d => xScale(new Date(d.Posted)))
    .attr('cy',d=>yScale(d.impressions))
        .attr('r', d => aScale(d.impressions))
        .attr('fill', 'red')

   //add circles to graph
    circles.enter()
        .append("circle")
        .attr('class', 'caco')
                .on('mouseover', (d, i, n) => {
            const selected=d3.select(n[i])
           
            handleMouseOver(selected,d)
        } )
        .on("mouseleave", (d, i, n) => {
            const selected = d3.select(n[i])
            handleMouseLeave(selected,d)
            
        })
        .attr('cx', 0)
        .attr('cy',gh)
        .transition()
        .duration(1000)
        
        
        .attr('cx', d => xScale(new Date(d.Posted)))
        .attr('cy', d => yScale(d.impressions))
        .attr('r', d => aScale(d.impressions))
        .attr('fill', 'red')

    
    //invoke the axis
    xgrp.transition().duration(1000).call(xaxis)
    ygrp.transition().duration(1000).call(yaxis)

    //style the y axis
    ygrp.selectAll(".ygrp text")
    .transition()
    .duration(1000)
        .attr('font-family', 'georgia')
        .attr('font-size', '1rem')
        .attr('fill', 'black')
        .attr('x',-20);
    //style the x axis
    xgrp.selectAll(".xgrp text")
      .transition()
      //.ease()
       .duration(1000)
       
        .attr('font-family', 'georgia')
        .attr('font-size', '1rem')
        .attr('fill', 'black')
       // .attr('transform', 'rotate(-60)')
        .attr('x', -30)
        .attr('text-anchor', 'middle');
    //label x axis
    graph.append('text')
        .text('Date')
        .attr("x", w / 3)
        .attr('y', gh + 80)
        .attr("letter-spacing",'5px')
        .attr('fill', 'teal')
        .attr('fill', 'black')
        .attr('font-family', 'helvetica');
    //label y axis
        graph.append('text')
        .text('Page impressions.')
        .attr("x",-gh/1.6)
            .attr('y',-75)
            .attr('transform', 'rotate(-90)')
            .attr("letter-spacing",'5px')
            .attr('fill', 'teal')
            .attr('font-weight','bold')
        .attr('font-family', 'helvetica');
    }
//get data
//set timers


//fetch data from file or api
async function getData(file){
    let data = await d3.csv(file)
    
    data
        .map(datam => {
           // datam.Posted=datam["Date"]
    datam.impressions = + datam['Lifetime Post Impressions by people who have liked your Page'];
    datam.totalImpressions = + datam['Lifetime Post Total Impressions'];
    datam.totalReach = + datam['Lifetime Post Total Reach'];
        return data;
    })
    //make dataset available globally
    dataset = data.slice(1)
    console.log(data)
    createGraph(dataset)
}
    //invoke the damn function
getData(file)




let sorted=false
const handleSortTable=()=> {
    sorted = !sorted;
    let datah=dataset.slice()
    if (sorted) {
         datah=datah.sort((a,b) => a.impressions - b.impressions)
        console.log(datah[0])
        createGraph(datah)  
    } else {
        createGraph(datah)
    }
    
}
let isLine = false;
const handleLine = () => {
    const btn=document.getElementById("sort-line")
    isLine = !isLine;
    isLine ? d3.select(".line").attr('stroke', 'red') : d3.select(".line").attr('stroke', 'none')
    isLine ? btn.textContent="Scatter Plot" :btn.textContent="Line Graph"
}

document.getElementById("sort-scatter").onclick = handleSortTable;
document.getElementById("sort-line").onclick = handleLine;