import * as d3 from "d3";
let sortTimer
let dataset = [];

export default function runPage(incomingData) {

    updateGraph(incomingData, "County", "Total_Population19");
    dataset = incomingData;

}


//dims
const h = 400;
const [w, gw] = getWidth();
let margin = { left: 100, right: 20, top: 20, bottom: 100 };


let gh = h - margin.bottom - margin.top;
const svg = d3.select("#census")
    .append("svg")
    .attr('height', h)
    .attr('width', w)
    .style("background", "#fff");

const graph = svg.append("g")
    .attr("height", gh)
    .attr("width", gw)
    .attr('transform', `translate(${margin.left},${margin.top})`)

/*SCALES AND AXIS */
//scales
const xScale = d3.scaleBand().rangeRound([0, gw]).paddingInner(.15).paddingOuter(0);
const yScale = d3.scaleLinear().rangeRound([gh, 0]);
//axis
const yaxis = d3.axisLeft(yScale).ticks(10).tickFormat(d => d > 0 ? formatNums(d) : "");
const xaxis = d3.axisBottom(xScale);


const ygrp = graph
    .append('g')
    .attr('class', 'ygrp')
const xgrp = graph
    .append('g')
    .attr('transform', `translate(0,${gh})`)
    .attr('class', 'xgrp');

function updateGraph(data, a, b) {

    //attach domain to scale
    xScale.domain(data.map(item => item[a]));
    yScale.domain([0, d3.max(data, d => d[b])]);
    //call the axis
    ygrp.transition().duration(1000).call(yaxis);
    xgrp.transition().duration(1000).call(xaxis);

    //add data
    const bars = graph.selectAll('rect')
        .data(data);
    bars.exit().remove();
    //update remaining bars
    bars
        .attr('height', 0)
        .attr('y', gh)
        .attr('x', d => xScale(d[a]))
        .transition().duration(1000)
        .attr('width', xScale.bandwidth)
        .attr('height', d => gh - yScale(d[b]))
        .attr('y', d => yScale(d[b]))
        .attr('fill', 'orange')

    bars
        .enter()
        .append('rect')
        .on('mouseover', handleMouseOver)
        .on('mouseleave', handleMouseLeave)
        .attr('width', xScale.bandwidth)
        .attr('x', d => xScale(d[a]))
        .attr('height', 0)
        .attr('y', gh)
        .transition().duration(1000)
        .attr('fill', 'orange')
        .attr('height', d => gh - yScale(d[b]))
        .attr('y', d => yScale(d[b]))

    //style the y axis
    ygrp.selectAll(".ygrp text")
        .transition().duration(1000)
        .attr("font-size", '1rem')
        .attr('font-family', 'helvetica')
        .attr('fill', 'black')
        .attr('x', '-15')
    //style the x axis
    xgrp.selectAll(".xgrp text")
        .transition().duration(1000)
        .attr("font-size", '1rem')
        .attr('font-family', 'helvetica')
        .attr('fill', 'black')
        .attr('transform', 'rotate(-60)')
        .attr('text-anchor', 'end')
        .attr('x', -10)
        .attr('y', '-5')
    //label x  axis
    graph.append('text')
        .text("COUNTY")
        .attr("x", w / 2.8)
        .attr('y', h - 30)
        .attr("font-size", '.75rem')
        .attr('font-family', 'helvetica')
        .attr('font-weight', 'bold')
        .attr('fill', 'black')
        .attr('letter-spacing', '10px')
    //label y axis
    graph.append('text')
        .text("POPULATION")
        .attr("x", -gh / 1.3)
        .attr('y', -70)
        .attr("font-size", '.75rem')
        .attr('font-family', 'helvetica')
        .attr('font-weight', 'bold')
        .attr('fill', 'black')
        .attr('letter-spacing', '10px')
        .attr('transform', 'rotate(-90)')

}




export function formatNums(num) {
    if (num >= 1e6) {
        return num / 1e6 + "M";
    } else {
        return num / 1e3 + "K";
    }
}



function handleMouseOver(d) {
    const selected = d3.select(this)
    selected.transition()
        .duration(200)
        .attr('fill', "blue")
    let x = Number(selected.attr('x'));
    let y = Number(selected.attr('y'));
    console.log('data', d)
    d3.select("#cens-tip")
        .style('top', y / 2 + "px")
        .style('left', x + "px")
        .style('opacity', 1)
    d3.select('.tip-county').text(d.County).attr('font-weight', "bold");
    d3.select('.tip-pop').text(d.Total_Population19.toLocaleString()).attr('font-weight', "bold");

}

function handleMouseLeave() {
    const selected = d3.select(this)
    selected.transition()
        .duration(200)
        .attr('fill', "orange")

    d3.select("#cens-tip")
        .style('opacity', 0)
}

let sorted = false;

function handleSort() {
    sorted = !sorted;
    let sort = dataset.slice();
    if (sorted) {
        sort = sort.sort((a, b) => a.Total_Population19 - b.Total_Population19);
        updateGraph(sort, "County", "Total_Population19");
    } else {
        updateGraph(sort, "County", "Total_Population19");
    }
}

sortTimer = setInterval(handleSort, 2000)


export function getWidth() {
    let w = document.documentElement.clientWidth;
    if (w <= 480) {
        return [320, 200];
    } else if (w <= 768) {
        return [600, 480];
    }
    else {
        return [w - 300, w - 420]
    }

}

const handleChange = () => {
    clearInterval(sortTimer)
    handleSort();
}

const sortEl = document.getElementById("sort");
if (sortEl) {
    sortEl.onchange = handleChange;
}



























