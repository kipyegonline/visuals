import * as d3 from "d3";
//define svg
//dimensions
const h = 400, w = 600, gh = 280, gw = 480;
let dataset = [];
const margin = { top: 20, left: 100, right: 20, bottom: 100 };
const svg = d3
 .select("#line-root")
.append('svg')
.attr("height",h)
.attr('width',w)
    .style("background", "lightblue")

const graph = svg.append('g')
    .attr("height", gh)
    .attr('width',gw)
    .attr('transform', `translate(${margin.left},${margin.top})`)
    //scales
    const xScale = d3.scaleTime().range([0, gw]);
const yScale = d3.scaleLinear().rangeRound([gh, 0]);
//axis
const yaxis = d3.axisLeft(yScale).ticks(5).tickFormat(d=> d >0 ? d.toLocaleString() :"")
const xaxis=d3.axisBottom(xScale)

const ygrp = graph
        .append('g')
        .attr('class','ygrp')
const xgrp = graph
    .append('g')
    .attr('transform', `translate(0,${gh})`)
    .attr('class','xgrp')

//EVENTS
const handleMouseOver = (selected, data) => {
    
    d3.select("#tip-view")
        .text(data.impressions.toLocaleString())
        .attr('font-weight', 'bold')
    let x = Number(selected.attr('cx')), y = Number(selected.attr('cy'));
    
    d3.select(".line-tip")
        .style('top', `${y + 20}px`)
        .style('left', `${ x - 20}px`)
        .style('opacity', 1)
    
}
const handleMouseLeave = (selected,data) => {
    d3.select(".line-tip").style('opacity',0)
}
function createLine(dataset) {
    xScale.domain(d3.extent(dataset, d => new Date(d.Posted)));
    yScale.domain([0, d3.max(dataset, d => d.impressions)]);

    const line = d3.line()
        .x(d => xScale(new Date(d.Posted)))
        .y(d => yScale(d.impressions));
    
    graph.append('path')
        .data([dataset])
        .transition()
        .duration(1000)
        .attr('class', 'line')
        .attr('d',line)
        .attr('fill', 'none')
        .attr('stroke','red')
        .attr('stroke-width', 2)
    //create circles
    const circles=graph.selectAll('circle')
    .data(dataset)
    //remove nwanted
    circles.exit().remove()
    //update current points
    circles.attr('r',4)
    .attr('cx',d=>xScale(new Date(d.Posted)))
    .attr('cy',d=>yScale(d.impressions))
    
    

//add new points
    circles.enter()
        .append('circle')
        .on('mouseover',(d,i,n)=>{
            d3.select(n[i])
            .transition()
            .duration(100)
            .attr('r',8)
                .attr('fill', 'blue')
            const selected= d3.select(n[i])
            handleMouseOver(selected,d)
        })
        .on('mouseleave',(d,i,n)=>{
            d3.select(n[i])
            .transition()
            .duration(100)
            .attr('r',4)
                .attr('fill', 'red')
                const selected= d3.select(n[i])
                handleMouseLeave(selected,d)
        })
    .attr('r',4)
    .attr('cx',d=>xScale(new Date(d.Posted)))
    .attr('cy',d=>yScale(d.impressions))
        .attr('fill', 'red')
        .append('text')
        .text(d => d.impressions.toLocaleString())
        .attr('x',d=>xScale(new Date(d.Posted)) + 5)
        .attr('y', d => yScale(d.impressions) + 5)
        .attr('fill', 'black')
        .attr('font-size', '1rem')
    

    //call the axis
    xgrp
        .transition()
        .duration(1000)
        .call(xaxis)
    ygrp
        .transition()
        .duration(1000)
        .call(yaxis)
    
    ygrp.selectAll('.ygrp text')
        .attr('font-size', '1rem')
        .attr('fill', 'black')
        .attr('font-family', 'helvetica')
        .attr('x', -30);
    xgrp.selectAll('.xgrp text')
        .attr('font-size', '1rem')
        .attr('fill', 'black')
        .attr('font-family', 'helvetica')
        .attr('transform', 'rotate(-60)')
        .attr('text-anchor','end')
        .attr('x', -10);
    
    graph.append('text')
        .text('DATE')
        .attr('x', w / 3)
        .attr('y', gh + 80)
        .attr('fill', 'teal')
        .attr('letter-spacing', '5px');
    
    graph.append('text')
        .text('Impressions')
        .attr('x', -gh /1.5)
        .attr('y', -70)
        .attr('letter-spacing', '5px')
        .attr('fill', 'teal')
        .attr('transform', 'rotate(-90)');
}



async function getData(file){
    let data = await d3.csv(file)
    
    data
        .map(datam => {
           // datam.Posted=datam["Date"]
    datam.impressions = + datam['Lifetime Post Impressions by people who have liked your Page'];
    datam.totalImpressions = + datam['Lifetime Post Total Impressions'];
    datam.totalReach = + datam['Lifetime Post Total Reach'];
        return data;
    })
    //make dataset available globally
    dataset = data.slice(1)
    console.log('line',dataset)
    createLine(dataset)
}
    //invoke the damn function

//events
//the main graph
//pull and clean data
//set timers