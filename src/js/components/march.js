const file = '../assets/Facebook Insights Data Export (Post Level) - Public Relations Society of Kenya - PRSK - 2019-04-05.csv'

//define svg dimaensions and data holder
let = h = 600, w = 600, dataset = [];
let timer;
const m = { l: 100, r: 20, t: 20, b: 100 };
const gw = w - m.l - m.r;
const gh = h - m.t - m.b;
//append svg to the DOM
const svg = d3.select("#march")
    .append('svg')
    .attr("width", w)
    .attr("height", h)
    .style("background", "lightblue");

    const graph=svg.append("g")
    .attr('width', gw)
    .attr('height', gh)
    .attr('fill','#ddd')
    .attr('transform', `translate(${m.l},${m.t})`)
    
    //scales; attach domain on upfate function
const xScale = d3.scaleBand().rangeRound([0, gw]).paddingInner(.1).paddingOuter(.1);
const yScale = d3.scaleLinear().rangeRound([gh, 0]);
    //axis
    const xaxis = d3.axisBottom(xScale).tickFormat(d=>new Date(d).toDateString());
const yaxis = d3.axisLeft(yScale).ticks(5);
    
//append group elemenys for x and y axis to SVG
const xgrp = graph.append('g')
    .attr('class', 'xgrp')
    .attr('transform', `translate(0,${gh})`)
    
    const ygrp = graph.append('g')
    .attr('class', 'ygrp')

//event handler for mouse over and mouse leave, on to up of main function since they are function expressions
const handleMouseOver = (d, selected) => {
    //clear interval
    clearInterval(timer)
    //get width of the bar
    const band = xScale.bandwidth()/2
            //get the tool tip top and left
            let x = Number(selected.attr("x")) + Number(xScale.bandwidth()),
                
                y = Number(selected.attr('y') / 2 + gh / 2);
    
    
            /* MUHIMU SANA select tooltip and append current coords */
            d3.select('#tooltip')
                .style('left', x + "px")
                .style('top', y + "px")
                .select("#reach")
                .text(`${d.totalReach.toLocaleString()}`)  
           //finally show tool tip     
    d3.select('#tooltip')
    .style('opacity',1)
    .classed('hidden',false)                     
}

const handleMouseOut = (d, selected) => {
    //start timer when mouse goes out
    timer = setInterval(handleSort, 2000);
    
        
    
    d3.select('#tooltip')
            
        .style('opacity',0)
}
//increment
const addEl=()=> {
    dataset[2].totalReach +=2000;
    console.log('Added')
    createGraph(dataset)
}
setTimeout(addEl,5000)
/*Butter and bread of the visualization */
function createGraph(dataset) {
    //attach scale to respective domain
    xScale.domain(dataset.map(data => data.Posted))
    yScale.domain([0, d3.max(dataset, d => d.totalReach)])
    
    const t=d3.transition().duration(200)
    
    //append rects
    const bars=graph.selectAll('rect')
        .data(dataset, d=>d['Post ID'])
       
       //remove exiting bars 
    bars.exit().remove();
    
  //update remaining bars
    bars
    .attr('x', d => xScale(d.Posted))
    .transition()    
    .duration(500)    
    .attr('y',d=>yScale(d.totalReach))
    .attr('height',d=>gh-yScale(d.totalReach))
     
    //
   


    bars
        .enter()
        .append('rect')        
        .on('mouseover', (d, i, n) => {
            const selected = d3.select(n[i])
            selected
            .transition()
            .duration(500)
                .attr('fill', 'blue')
            //call the event for more
            handleMouseOver(d,selected)
            
            
        })
        .on('mouseleave', (d, i, n) => {
            
            const selected=d3.select(n[i])
            selected
            .transition()
            .duration(200)
                .attr('fill', 'blue')
                .attr('fill', 'orange');
            
            handleMouseOut(d,selected)
        })
        .attr('y',gh)
        .attr('height',0)
        .transition()        
        .duration(500)       
        .attr('x',d=>xScale(d.Posted))
        .attr('y',d=>yScale(d.totalReach))
        .attr('height',d=>gh-yScale(d.totalReach))
        .attr('width',xScale.bandwidth)
        .attr('fill', 'orange')
    
      
    //select and append texts to svg
    const texts = graph
        .selectAll('.mid-text')
        .data(dataset);
        
        
      //remove used texts elememnts
        texts.exit().remove()
    
    
  //update remaining texts elements
    
    texts
    .text(d => `${d.totalReach.toLocaleString()}`)    
        .transition().duration(1000)
        .attr('y', d => yScale(d.totalReach) +15 )
        .attr('x',(d,i)=>xScale(d.Posted) + xScale.bandwidth(i)/2 )        
        .attr('fill', 'white')
        .attr('text-anchor', 'middle')
        .attr('font-family', 'georgia')
        .attr('font-size', '1rem')

       

  //finally append texts
    
    
        texts
        .enter()
        .append('text') 
        .text(d => `${d.totalReach.toLocaleString()}`)
        .attr('x',0)   
        .attr('y',gh)
             .transition().duration(1000)  
            .attr('class','mid-text')
        .attr('x',(d,i)=>xScale(d.Posted) + xScale.bandwidth(i)/2 )
        .attr('y', d => yScale(d.totalReach) +15 )
        .attr('fill', 'white')
        .attr('text-anchor', 'middle')
        .attr('font-family', 'georgia')
        .attr('font-size', '1rem')
        ygrp.transition().duration(1000).call(yaxis)
        xgrp.transition().duration(1000).call(xaxis)
        
    
    
    
//style the texts on y axis
    ygrp.selectAll('.ygrp text')
        .attr('fill', 'teal')
        .attr('font-family', 'cursive')
        .attr('font-size','1rem')
        .attr("x",-20)
       
    
    //style the text on x axis
        xgrp.selectAll('.xgrp text')
        .attr('fill', 'teal')
        .attr('font-family', 'cursive')
        .attr('transform', 'rotate(-60)')
            .attr('text-align', 'center')
            .attr('font-size','1rem')
              .attr('x',-20)

  
    //label the x axis
    graph.append('text')
        .text('POSTS')
    .attr('x',gw/3)
    .attr('y',gh +80)
        .attr('fill', 'black')
    //label the y axis
        graph.append('text')
        .text('Impressions')
        .attr('x',-(gw/2))
            .attr('y', -70)
            .attr('font-size','1rem')
        .attr('fill', 'black')
        .attr('transform', 'rotate(-90)')  
    
        
}
    

    //fetch data from file or api
async function getData(file){
    let data = await d3.csv(file)
    
    data
        .map(datam => {
           // datam.Posted=datam["Date"]
    datam.impressions = + datam['Lifetime Post Impressions by people who have liked your Page'];
    datam.totalImpressions = + datam['Lifetime Post Total Impressions'];
    datam.totalReach = + datam['Lifetime Post Total Reach'];
        return data;
    })
    //make dataset available globally
 dataset=data.slice(1)

}
    //invoke the damn function
getData(file)

//run graph after half a second
setTimeout(() => createGraph(dataset), 500)
//use boolean for sorting
let sort = false
//button event handler
const handleClick = () => { clearInterval(timer); handleSort() };
//sorting func
const handleSort = () => {
    
    sort = !sort;
    let sorted=dataset.slice();
    
    if (sort) {
        sorted = sorted.sort((a, b) => a.totalReach - b.totalReach);
       
        createGraph(sorted)
    } else {
        
        createGraph(sorted)
    }
    
    
   
}
document.querySelector('.sort').addEventListener('click', handleClick)

  timer=setInterval(handleSort,2000)