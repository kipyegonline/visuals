import * as d3 from "d3";
import { getWidth, formatNums } from "./feb";
export function parseData(data){
    //2019-06-20 16:54
    const formatTime=d3.timeParse('%Y-%m-%d %H:%M');
    return data.map(item=>{
        item.impressions=+ item.impressions
        let date=item.time.split(' ').slice(0,2).join(' ');
        item.time=formatTime(date);
        
        return item
    })
}


export default function charting(data){
    updateSCatter(data,"Male populatio 2019","Female population 2019");
    updateData(data,"Total_Population19","County")

    
    

}

const colorPicker=(d)=>{
    if(d<15000){
        return 'red'
    }else if(d<20000){
        return 'skyblue'
    }else if(d<25000){
        return 'yellow'
    }else{
        return 'teal'
    }
}
//dimensions
const h =500;

const margin={left:50,bottom:100,top:20,right:20};
const gh=h-margin.left-margin.right;
const [w,gw]=getWidth();

//scales
const yScale=d3.scaleLinear().range([gh,0]);
const xScale=d3.scaleBand().range([0,gw]);
const svg=d3.select('#canvas')
.append('svg')
.attr('width',w)
.attr('height',h)
.style('background','#ddd');

const graph=svg.append('g')
.attr('height',gh)
.attr('width',gw)
.attr('transform',`translate(${margin.left},${margin.top})`)

//axes
const xgrp=graph.append('g')
.attr('class','xgrp')
.attr('transform',`translate(0,${gh})`)
const xaxis=d3.axisBottom(xScale)
const ygrp=graph.append('g')
.attr('class','ygrp')
const yaxis=d3.axisLeft(yScale)

function updateData(data,a,b){
  yaxis.tickFormat(data=> formatNums(data));
    yScale.domain([0,d3.max(data,
        d=>d[a])
    ]);
    xScale.domain(data.map(item=>item[b])).paddingInner(.2).paddingOuter(.15)
    //append data
    const bars=graph.selectAll('rect')
    .data(data);
    bars.exit().remove();
    //update existing bars
bars.attr('x',d=>xScale(d[b]))
.attr('y',d=>yScale(d[a]))
.attr('width',xScale.bandwidth)
.attr('height',d=>gh-yScale(d[a]))
//add new rects
    bars
    .enter()
    .append('rect')
    .on('mouseover',(d,i,n)=> {
       const x= n[i]
       n[i].style.fill='red'
      
        
    })
    .on('mouseleave',(d,i,n)=> n[i].style.fill='deepskyblue')
    .attr('y',gh)
    .attr('height',0)
    .transition()
    .duration(1000)
    .ease(d3.easeBounceIn)
    .attr('x',d=>xScale(d[b]))
    .attr('y',d=>yScale(d[a]))
    .attr('width',xScale.bandwidth)
    .attr('height',d=>gh-yScale(d[a]))
    .attr('fill',d=>"deepskyblue")
    //finally call axis
    ygrp.transition().duration(1000).call(yaxis)
    xgrp.transition().duration(1000).call(xaxis)
    //style text
    ygrp.selectAll('text')
    .transition().duration(1000)
    .attr('fill','black')
    .attr('font-family','garamond')
    .attr('font-size','.75rem')
    .attr('font-weight','bold')

    xgrp.selectAll('text')
    .transition().duration(1000)
    .attr('fill','black')
    .attr('transform','rotate(-40)')
    .attr('font-family','garamond')
    .attr('font-size','.75rem')
    .attr('font-weight','bold')
    .attr('text-anchor','end')

}
//scatter plot


//append svg
const scatter=d3.select('#scatter')
.append('svg')
.attr('width',w)
.attr('height',h)
.style('background-color','gray')
//append group
const chart=scatter.append('g')
.attr('height',gh)
.attr('width',gw)
.attr('transform',`translate(${margin.left},${margin.top})`)
//scales
const y=d3.scaleLinear().range([gh,0]);
const x=d3.scaleLinear().range([0,gw])
const aScale=d3.scaleSqrt().range([5,20])
//axes
const xax=d3.axisBottom(x);
const yax=d3.axisLeft(y).ticks(5);
const xrp=chart.append('g')
.attr('class','scatter-x')
.attr('transform',`translate(0, ${gh})`)


const yrp=chart.append('g')
.attr('class','scatter-y')

function updateSCatter(data,a,b){
    y.domain(d3.extent(data,d=>d[a]));
    x.domain(d3.extent(data,d=>d[b]))
    aScale.domain(d3.extent(data,d=>d[a]));
    
    //bind data
    const circles=chart.selectAll('circle')
    .data(data)
    //exit circles
    circles.exit().remove()
    //update existing
    circles.attr('cy',d=>y(d[a]))
    .attr('cx',d=>x(d[b]))
    .attr('x',d=>x(d[b]))
    .attr('r',d=>aScale(d[a]))
    .attr('y',d=>x(d[b]))
    //enter new
    circles.enter()
    .append('circle')
    .on('mouseover',(d,i,n)=>{
        n[i].style.fill='green'
        const hio=d3.select(n[i])
        
        hio.raise()
    })
    .attr('cy',d=>y(d[a]))
    .attr('cx',d=>x(d[b]))
    .attr('x',d=>x(d[b]))
    .attr('r',d=>aScale(d[a]))
    .attr('y',d=>x(d.time))
    .attr('fill',d=>  d[a]>1000 ?'skyblue':'red')
    
    //call axes
    xrp.call(xax)
    yrp.call(yax)
    chart.append('text')
    .attr('x',gw/2)
    .attr('y',gh + 35)
    .attr('fill','black')
    .text('Jules')

    xrp.selectAll('text')
    .attr('transfrom','rotate(-90)')

}


