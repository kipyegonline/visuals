import * as d3 from "d3";
import { entries } from "d3";
let dataset=[]
export function Pie(data){
    dataset=data;
   
    updateData(data)
    setTimeout(()=>updateData(data.slice(0,40)),5000)
    setTimeout(()=>updateData(dataset),7000)
}



const dims={height:300,width:300,radius:150};
const cent={x:(dims.width/2 +60), y:dims.height/2+50}

const svg=d3.select('#pie-root')
.append('svg')
.attr('width',dims.width +150)
.attr('height',dims.height +150)
.style('background','lightblue')

const graph=svg.append('g')
.attr('transform',`translate(${cent.x},${cent.y})`);

const pie=d3.pie()
          .sort(null)
          .value(d=>d.Total_Population19);
          
const arcPath=d3.arc()
.outerRadius(dims.radius)
.innerRadius(dims.radius/2);
const color=d3.scaleOrdinal(d3.schemeCategory10);
//legends
const lg=svg.append('g')
.attr('transform',`translate(${dims.width+40},${10})`)





  function updateData(data){
     
      color.domain(data.map(item=>item.County))
const paths=graph
.selectAll('path')
.data(pie(data));

paths.exit()
.transition().duration(500)
.attrTween('d',archTweenExit)
.remove();

paths.attr('d',arcPath)
.transition()
.duration(500)
.attrTween('d',arcTweenUpdate)


paths
.enter()
.append('path')
.attr('class','arc')
.attr('stroke','#fff')
.attr('stroke-width',2)
.attr('fill',(d,i)=>color(i))
.each(function(d){
    this._current=d
})
.transition().duration(500)
.attrTween('d',archTweenEnter)
  }


const archTweenEnter=d=>{
    const i =d3.interpolate(d.endAngle,d.startAngle);
return t=> {
    d.startAngle=i(t)
return arcPath(d)};}

const archTweenExit=d=>{
    const i =d3.interpolate(d.startAngle,d.endAngle);
return t=> {
    d.startAngle=i(t)
return arcPath(d)};}

function arcTweenUpdate(d){
//innterpolate
const i=d3.interpolate(this._current,d);
//update current with new 
this._current=i(1);
return function(t){
    return arcPath(i(t))
}
}